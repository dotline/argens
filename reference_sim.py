'''
reference_sim.py
generates mutant references
RefSim class stores data related to the mutated reference

dependencies:
snpmutator must be installed
snpmutator : https://github.com/CFSAN-Biostatistics/snp-mutator
biopython

Justin Evan Kincaid <jkincaid@nau.edu>
12/2016
'''

from Bio import SeqIO
import os
import subprocess
import glob
import json
from collections import OrderedDict

#store records of mutated references for convenience
#generates a mutated reference using snpmutator
class RefSim:

    #create the mutated reference
    #store its relevant parameters in private class variables
    def __init__(self, path_to_reference,output_path,test_name,ins,dels,subs,random_seed):

        self.__records = list(SeqIO.parse(path_to_reference,"fasta"))

        self.__file_ids = []

        self.__mimetype = path_to_reference.rsplit(".")[-1]

        self.__ref_id = path_to_reference.rsplit("/")[-1].rstrip(".fa").rstrip(".fasta")

        self.__out_path = output_path

        self.__tmp_path = "tmp_mutants"

        self.__reference_path = path_to_reference.rstrip("%s.%s" %(self.__ref_id,self.__mimetype))
        #debug
        #print("this is the path %s" %self.__reference_path)
        self.__summary_json = OrderedDict()
        self.__summary_json["mutant_genome"] = []

        #make temp directory for mutant genomes
        #don't cry if it already exist
        try:
            os.mkdir(self.__tmp_path)
        except Exception as e:

            pass
            #we don't really need to know if it already exist
            #print(e)

        #debug
        #print(self.__ref_id)

        #check for multi fasta records if found split them up
        #because snpmutator needs a file not a sequence *arggggg*
        if len(self.__records) > 1:
            self.__split_files(self.__records,self.__tmp_path,test_name)
            self.__reference_path = self.__tmp_path

        else:

            self.__file_ids.append(self.__ref_id)

        #for each record create a mutated genome
        for index, record in enumerate(self.__records):

            tmp_len = len(record.seq)

            tmp_ins = str(int(tmp_len*ins))

            tmp_del = str(int(tmp_len*dels))

            tmp_sub = str(int(tmp_len*subs))

            self.__mutate(self.__file_ids[index],test_name, self.__reference_path, self.__out_path,
                            tmp_ins, tmp_del, tmp_sub,str(random_seed))

        '''move mutated genomes to correct output path
           assumes all previous mutated genomes have been moved
           to their final destination directory
           will be fixed when homebrewed mutator is written
        '''
        tmp_ids = []
        for tfile in glob.glob("%s*_mutated*" % (self.__ref_id)):
            #JUST MOVE the previous files for now
            # if test_name or "fastq" or "json" or "tsv" in tfile:
            #     print("already exist")
            # else:
            #debug
            #print("this is the file %s" % tfile)
            tmp_name = "%s_%s.%s" %(tfile.rstrip(".fasta"),test_name,"fasta")
            tmp_ids.append(tmp_name)
            #debug
            #print("this is the files final name %s" % tmp_name)
            os.rename(tfile, "%s/%s_%s.%s" %(output_path,tfile.rstrip(".fasta"),test_name,"fasta"))

        #this is tempory fix and very dumb
        #only used because work around with snp mutator
        #once doing self made mutations will delete
        self.__file_ids = tmp_ids


    #create fasta file for each sequence in multi fasta file
    #wrap around, snp mutator must have seperate file for
    #each sequence or it just concatenates all the sequences
    #in multi fasta file
    def __split_files(self,records_list, tmp_dir,test_name):

        for index, record in enumerate(self.__records):

            #debug
            #print(index,record.id, len(record.seq))

            tmp_id = "%s_%i" %(self.__ref_id,index)

            out_path_str = "%s/%s.%s" %(tmp_dir,tmp_id,self.__mimetype)

            self.__file_ids.append(tmp_id)

            with open(out_path_str,"w") as ofile:
                SeqIO.write(record,ofile,"fasta")

        #debug
        #print(self.__file_ids)

    #call snpmutatory.py generate a mutated reference genome
    def __mutate(self,file_id,test_name,reference_path, output_path,ins,dels,subs,random_seed):

        snp_mutator_run = subprocess.run(["snpmutator.py",
                                          "-r", random_seed,
                                          "-n", "1",
                                          "-i", ins,
                                          "-d", dels,
                                          "-s", subs,
                                          "-o", "%s/%s_summary_%s.tsv" %(output_path,file_id,test_name),
                                          "%s/%s.%s" %(reference_path,file_id,self.__mimetype)],
                                          stdout=subprocess.DEVNULL)

        self.__generate_json_summary( file_id, test_name, reference_path,
                                output_path, ins, dels, subs, random_seed)
        #debug and success msg
        #print("%s successfully mutated" % file_id)

    #generate a json summary of the mutated reference
    def __generate_json_summary(self, file_id,test_name, reference_path,
                            output_path,ins,dels,subs,random_seed):

            tmp_json = {}
            tmp_json["mutant_genome"] = OrderedDict({    "mutant_reference" :
                                             {"origin_id" : self.__ref_id,
                                              "mutant_id" : "%s_mutated_1_%s" %(file_id,test_name),
                                             "mutator" : "snpmutator.py",
                                             "mutation_summary": "%s/%s_summary_%s.tsv" %(output_path,file_id,test_name),
                                             "subs": subs,
                                             "ins": ins,
                                             "dels" : dels,
                                             "random_seed" : random_seed,
                                             "test_id" : test_name }
                                             })

            self.__summary_json["mutant_genome"].append(tmp_json["mutant_genome"])

    #remove if necessary all the files in the tmp directory
    def clean_up(self):
        #clean up temporary files created for snpmutatory for multifasta
        for tmpfile in glob.glob("%s/*" % self.__tmp_path):

            os.remove(tmpfile)

        os.rmdir(self.__tmp_path)

    #list of mutant file id's associated with the original reference
    @property
    def file_ids(self):
        return self.__file_ids

    #path were mutant references were stored
    @property
    def output_directory(self):
        return self.__out_path

    #json formatted string summary of reference mutations
    @property
    def json_summary(self):
        return json.dumps(self.__summary_json, sort_keys=False,indent=4)

    #the original reference_id for the mutant genome/genomes
    @property
    def ref_id(self):
        return self.__ref_id
