'''
simulate_reads.py
SimReads class stores information on simulated read set for reference
pointed to by the reference path

dependencies:
simlord must be installed for SimReads to create read set
simlord    : https://bitbucket.org/genomeinformatics/simlord

Justin Evan Kincaid <jkincaid@nau.edu>
'''
import json
import subprocess


class SimReads:

    #store params in private class variables, then simulate the reads
    def __init__(self, reference_path, output_path,number_of_reads,
                read_length, probability_ins, probability_del, probability_sub):

        self.__reference_id = None

        self.__summary_json = None

        #open the reference path this verifies if the file exist
        with open(reference_path,"r") as ref_file:

            self.__reference_id = ref_file.name.rstrip(".fasta").rstrip("fa")

        self.__output_summary = {}
        #simlord parameter calls
        self.__simlord_args = [ "simlord",
                                "--read-reference",
                                "-n",
                                "-fl",
                                "-pi",
                                "-pd",
                                "-ps",
                                "--no-sam"]

        #generate output path if None was given as a parameter, ideally this is
        #best option, because simlord
        if output_path is None:

            output_path = reference_path.rstrip(".fa").rstrip(".fasta") + "_" + str(number_of_reads) + "_reads"

        self.__reads_header = output_path

        self.__simulate(reference_path, output_path,str(number_of_reads),
                str(read_length), str(probability_ins), str(probability_del), str(probability_sub))

        #more or less for debug, could be usefull but not necessary
        self.__dump_json_summary(output_path, number_of_reads,
               read_length, probability_ins, probability_del, probability_sub)


    #simulate the readset using a subprocess call to simlord
    def __simulate(self, reference_path, output_path,number_of_reads,
                read_length, probability_ins, probability_del, probability_sub):

        sp_run = subprocess.run([self.__simlord_args[0],
                                self.__simlord_args[1], reference_path,
                                self.__simlord_args[2], number_of_reads,
                                self.__simlord_args[3], read_length,
                                self.__simlord_args[4], probability_ins,
                                self.__simlord_args[5], probability_del,
                                self.__simlord_args[6], probability_sub,
                                self.__simlord_args[7],
                                output_path],
                                stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
        #debug
        #print(sp_run.args)

    #generate a json formatted summary and write it to file
    def __dump_json_summary(self,output_path, number_of_reads, read_length,
                            probability_ins, probability_del, probability_sub):


        tmp_json_summary= {}

        #really shouldn't change any of this EVER
        tmp_json_summary["mutant_reads"] = { "mutant_read_set":{
                                             "id"   : self.__reference_id,
                                             "reads" : {
                                                  "file" : output_path+".fastq",
                                                  "total_reads" : number_of_reads,
                                                  "read_length" : read_length,
                                                  "error_rates" : {"ins" : probability_ins,
                                                                   "del" : probability_del,
                                                                   "sub" : probability_sub
                                                                   }
                                                      }
                                                  }
                                              }

        #store summary to grab later and easier
        self.__summary_json = json.dumps(tmp_json_summary, sort_keys=False, indent=4)
        with open(output_path+".json","w") as jsonFile:

            json.dump(tmp_json_summary,jsonFile, sort_keys=False)


    #json formatted string summary of read set
    @property
    def summary_json(self):
        return self.__summary_json

    #the file header/name of generated read set
    @property
    def test_header(self):
        return self.__reads_header
