#ARGENS README #

**SERIOUSLY READ THIS**

This README actually tries to explain how to use this software and how this software can and will break. 

### What is Argens? ###

* Artificial reference generator and read simulator 
* Metagenomic pipeline test generator
* Wrapper script for [snp-mutator](https://github.com/CFSAN-Biostatistics/snp-mutator) and [simlord](https://bitbucket.org/genomeinformatics/simlord)
* Version 0.01


### How do I get set up? ###

Argens is a python command line script therefore
one must have at least **Python3.+** installed for it to do anything.

### Dependencies ###
* [simlord](https://bitbucket.org/genomeinformatics/simlord)
* [snp-mutator](https://github.com/CFSAN-Biostatistics/snp-mutator)
* [numpy](http://www.numpy.org/)
* [scipy](http://biopython.org/wiki/Documentation)
* [cython](http://cython.org/)
* [pysam](https://github.com/pysam-developers/pysam)
* [dinopy](https://bitbucket.org/HenningTimm/dinopy)

## USE [CONDA](http://conda.pydata.org/docs/using/index.html) ##
* there are too many dependencies \*applies duct tape\* 
* **please use a conda environment**
####Universal Configuration
Install latest version of [Anaconda](https://www.continuum.io/downloads)

create the Conda environment with all the right ingredients


```

conda create -m -p /path/to/virt/env/argens python=3 pip numpy scipy cython 

#activate the environment

source activate /path/to/virt/env/argens

#install the other dependencies with pip

pip install pysam dinopy simlord snp-mutator

#if everything was successful *praise the sun*
#go ahead and leave the environment

source deactivate /path/to/virt/env/argens

```

>you will need to be in this environment every time you run argens.py
>
>use : 
>
>`source activate /path/to/env/argens`
>
>to start the environment

#### *so now you need argens*  
clone the argens git repo
```

#move to directory where you want to keep argens
cd /path/to/argens

#clone the repo
git clone https://bitbucket.org/dotline/argens.git 

```


###Usage

Activate your CONDA environment you created earlier
```
source activate /path/to/env/argens
```

*once the virtual environment is activated you can now run argens.py*

you can either go to the root directory and run argens like the following examples

or you can substitute `python3 argens.py` with `python3 /path/to/argens.py` 

either way argens does not have an install script yet so just go to the root directory you stored the argens git repo


```
python3 argens.py
usage: argens.py [-h] [--gen-reference | --gen-reads | --gen-config N]
                 CONFIG.JSON
```

If you forget how to use argens just use the '-h' parameter

```
argens]$ python3 argens.py -h
usage: argens.py [-h] [--gen-reference | --gen-reads | --gen-config N]
                 CONFIG.JSON

artificial genome reference generator and read simulator

positional arguments:
  CONFIG.JSON      configuration file

optional arguments:
  -h, --help       show this help message and exit
  --gen-reference  generate mutant references specified in config file
  --gen-reads      generate read set from reference specified in config file
  --gen-config N   generate config template with N number of references

the Milli Vanilli of w[rapper] scripts
```

>Argens depends upon a JSON formatted configuration file to achieve any of its task
>
>luckily argens will generate a skeleton config file for you

###argens.py --gen-config

go to the root directory of argens

```
python3 argens.py --gen-config N name_of_config.json
```
Where **N** is the number of genomic references that will be used to generate the test sets, and **name_of_config.json** is the prefix of the output file that will be generated.

* The output file will be named in the format 

    * `[name_of_config]_config_template_[timestamp].json`

The config_template is just a skeleton template, it will do **nothing** , if you run argens **without** filling out the entries in the config file **IT WILL BREAK**

###argens.py --gen-reference

*generate mutant references of the base references specified in the CONFIG.JSON file.*

```
python3 argens.py --gen-reference /path/to/config.json
```

* --gen-reference will write references files in the base directory and then move them to a directory named with the **'test name'** parameter specified in the config file. 

* it produces two output files. On file being the mutant reference, the other being a \*.tsv file that stores the mutations performed on the original reference. 

###argens.py --gen-reads

*generates a read set based on the references specified in the CONFIG.JSON file.* 

```
python3 argens.py --gen-reads /path/to/config.json
```

* writes read set to individual files for reads then cleans them up after output[simlord]
* outputs the reads to **stdout** 


###argens.py CONFIG.JSON

*generates mutant references and then from those mutant references generates read set according to parameters in CONFIG.JSON. 

```
python3 argens.py /path/to/config.json
```

* stores mutant references and read sets in working directory in folder named with **'test name'** parameter in CONFIG.JSON.
* outputs read set to stdout

>this is the whole !shebang will do everything but clean the kitchen sink

**Don't forget to leave your CONDA  environment

```
source deactivate /path/to/env/argens
```
###Fofanov lab setup ###
* just use the created environment on monsoon 
* @ /scratch/jek243/conda/env/argens

##DISCLAIMER

>*this is an I/O nightmare*
>
>* *this is a wrapper around existing tools*
>
>*the only way the tools output their data is to write to file*


### Who do I hold responsible for this mess? ###

* ####*Justin Evan Kincaid* 
    * Northern Arizona University SICCS undergraduate 

     * email *jkincaid@nau.edu*