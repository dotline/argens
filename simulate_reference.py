import random
import json
from datetime import datetime

class ReferenceMutator:


    def __init__(self, path_to_reference, test_name, similarity_percentage):

        instance_time = datetime.now().strftime(">%d%m%y%H%M%S_")
        self.__summary_json = None
        self.__reference_header = None
        self.__reference_id = None
        self.__reference_sequence = ""

        #mutant_id ref_id len num_of_subs similarity_percentage
        self.__mutant_header = instance_time

        self.__mutant_sequence = None

        self.__mutant_id = instance_time.strip(">")

        self.__number_of_samples = 0



        with open(path_to_reference,'r') as ref_file:
            self.__reference_header = ref_file.readline()
            if self.__reference_header[0] == ">":
                self.__reference_id = self.__reference_header.split()[0].strip(">")
                print(self.__reference_id)
                self.__mutant_header += self.__reference_id
                self.__mutant_id += self.__reference_id
            else:
                raise TypeError("Incorrectly Formatted Fasta Header")

            for line in ref_file.readlines():

                self.__reference_sequence += line.strip("\n")

        self.__seq_len = len(self.__reference_sequence)

        self.__test_name = self.__mutant_id +"_" + test_name

        if similarity_percentage is not None:
            self.__sim_percent = (100 - similarity_percentage)/100
        else:
            self.__sim_percent = 0.02

        print(self.__sim_percent)

        self.__replacement_set = set(["A","C","T","G"])
        self.__replacement_list = {}

        self.__mutate()
        self.__write_mutant_reference()
        self.__dump_json_summary(test_name)

    def __mutate(self):

        self.__mutant_sequence = self.__reference_sequence

        self.__number_of_samples = random.sample(range(self.__seq_len), int(self.__seq_len * self.__sim_percent))

        for i in self.__number_of_samples:

            tmp_seq_char = self.__reference_sequence[i]
            tmp_seq_set  = set(tmp_seq_char)^self.__replacement_set
            tmp_sub_char = random.choice(list(tmp_seq_set))

            self.__replacement_list[i+1] = [tmp_seq_char,tmp_sub_char]

            self.__mutant_sequence = self.__mutant_sequence[:i] + tmp_sub_char + self.__mutant_sequence[i+1:]

    def __write_mutant_reference(self):

            with open(self.__test_name+".fa",'w') as mfile:

                #mutant_id ref_id len num_of_subs similarity_percentage test_name

                mfile.write("{0} {1} {2} {3} {4}\n".format(self.__mutant_header, self.__reference_id,
                                                        self.__seq_len, len(self.__number_of_samples),
                                                        (100 - (100*self.__sim_percent)) ))
                tmp_count = 0
                chunk = 77
                for c in self.__mutant_sequence:
                    if tmp_count != 0 and tmp_count%chunk == 0:
                        mfile.write("\n")
                    tmp_count += 1
                    mfile.write(c)

    def __dump_json_summary(self, test_name):

            tmp_json = {}
            tmp_json["mutant_genome"] = {    "mutant_reference" :
                                             {"origin_id" : self.__reference_id,
                                             "id" : self.__mutant_id,
                                             "subs": self.__replacement_list,
                                             "seq_len" : self.__seq_len,
                                             "sub_count" : len(self.__number_of_samples),
                                             "similarity" : (100 - (100*self.__sim_percent)),
                                             "test"       : test_name }
                                             }
            with open(self.__test_name + ".json", 'w') as jfile:

                json.dump(tmp_json, jfile, sort_keys=True)

            self.__summary_json = json.dumps(tmp_json, sort_keys=True, indent=4)






    @property
    def summary_json(self):
        return self.__summary_json

    @property
    def test_header(self):
        return self.__test_name
