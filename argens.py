'''
Argens -- Artificial reference generator and read simulator

Generate artificial genomic sequence from a reference sequence
Generate simulated read set from reference sequence
Generate a json formatted configuration file

Depends on
snpmutator : https://github.com/CFSAN-Biostatistics/snp-mutator
simlord    : https://bitbucket.org/genomeinformatics/simlord

argens files:
reference_sim.py
simulate_reads.py
generate_config.py

which all have multiple dependencies see readme at
https://bitbucket.org/dotline/argens


Justin Evan Kincaid <jkincaid@nau.edu>

'''

import argparse
import sys
import os
from reference_sim import RefSim
from simulate_reads import SimReads
from generate_config import ConfigGen
import json
import random
import itertools
from numpy.random import multinomial
import glob
from Bio import SeqIO
from Bio.SeqRecord import SeqRecord
from collections import OrderedDict
'''
generate mutant references from [filename].json file
returns a list of RefSim objects
'''
def generate_references(filename):

    conf_file = None

    with open(filename, "r") as conf_file:

        conf_file = json.load(conf_file)

    test_vars = conf_file['test variables']

    ref_list = conf_file['individual params']

    mutant_list = []

    # should verify if entries exist
    # *bubblegum* make it work
    for record in ref_list:
        mutant_list.append(RefSim(
            record['reference']['reference_path'],
            test_vars['file locations']['working_dir'],
            test_vars['test_name'],
            record['reference']['mutator_params']['substitution_ratio'],
            record['reference']['mutator_params']['insertion_ratio'],
            record['reference']['mutator_params']['deletion_ratio'],
            random.randint(0, 100)
        ))

    # for mutant in mutant_list:
    #     print(mutant.file_ids)

    return mutant_list

'''
generate read set from config [filename].json
references are given in the config file
assumes all references exist
returns a list of SimRead objects
'''
def generate_reads_from_config(filename):

    with open(filename, "r") as conf_file:

        conf_file = json.load(conf_file)

    test_vars = conf_file['test variables']

    ref_list = conf_file['individual params']

    reads_list = []

    # should verify if entries exist
    # *bubblegum* make it work
    #@todo need to handle multi fasta files
    for record in ref_list:
        reads_list.append(SimReads(
            record['reference']['reference_path'],
            None,  # should be output file name but we generate it in SimRead class
            int(test_vars['total_number_of_reads'] * record['reference']['read_params']['total_set_ratio']),
            record['reference']['read_params']['read_length'],
            record['reference']['read_params']['substitution_ratio'],
            record['reference']['read_params']['insertion_ratio'],
            record['reference']['read_params']['deletion_ratio']
        ))
    # for read in reads_list:
    #     print(read.test_header)

    return reads_list

'''
generate read set from config [filename].json and list of RefSim objects
references are matched from RefSim objects to records in config file_id
the read sets are then generated with defined parameters in the matching
record of the config file
assumes all references exist
returns a list of SimRead objects
'''
def generate_reads_from_mutant(filename, mutant_list):

    with open(filename, "r") as conf_file:

        conf_file = json.load(conf_file)

    test_vars = conf_file['test variables']

    ref_list = conf_file['individual params']

    reads_list = []

    # should verify if entries exist
    # *bubblegum* make it work
    #for all mutants find their matching record in config
    #use parameters from config to generate the read set for that reference
    for mutant in mutant_list:
       for record in ref_list:
           #handle multiple fasta file
           #build correct set ratios
           if mutant.ref_id in record['reference']['reference_path']:
               total_reads = int(test_vars['total_number_of_reads'] * record['reference']['read_params']['total_set_ratio'])
               num_of_seq = len(mutant.file_ids)
               num_of_reads = list(multinomial(total_reads,[1/float(num_of_seq) ]*num_of_seq))
               for mut_id in mutant.file_ids:
                    reads_list.append(SimReads(
                        mut_id,
                        None,  # should be output file name but we generate it in SimRead class
                        int(num_of_reads.pop()),
                        record['reference']['read_params']['read_length'],
                        record['reference']['read_params']['substitution_ratio'],
                        record['reference']['read_params']['insertion_ratio'],
                        record['reference']['read_params']['deletion_ratio']
                    ))

    # for read in reads_list:
    #     print(read.test_header)

    return reads_list

def generate_reads_no_errors(filename):

    with open(filename, "r") as conf_file:
        conf_file = json.load(conf_file)

    test_vars = conf_file['test variables']

    ref_list = conf_file['individual params']

    generated_reads_list = []
    for obj in ref_list:
        read_length = obj["reference"]["read_params"]["read_length"]
        with open(obj["reference"]["reference_path"], "r") as seqFile:

            for genome_record in SeqIO.parse(seqFile,"fasta"):
                print(len(genome_record.seq), read_length)
                total_reads = int(test_vars["total_number_of_reads"]*obj["reference"]["read_params"]["total_set_ratio"])
                randselect = random.sample(range(len(genome_record.seq)-read_length),total_reads)
                for i in range(total_reads):
                    start = random.choice(randselect)
                    end = start + read_length -1
                    seqfrag = genome_record.seq[start:end]
                    seq_rec = SeqRecord(seqfrag,"{0}:read_{1}".format(genome_record.id,i+1),"","")
                    generated_reads_list.append(seq_rec)

    SeqIO.write(generated_reads_list,test_vars["test_name"]+".fasta","fasta")



'''
move all files created from working dir to their own
test_name directory
'''
def clean_up(config_name):

    with open(config_name, "r") as conf_file:

        conf_file = json.load(conf_file)

    test_vars = conf_file['test variables']

    ref_list = conf_file['individual params']

    test_name = test_vars['test_name']

    try:
        os.mkdir("%s" %test_name)
    except:
        pass

    for item in glob.glob("*"+test_name+"*"):
        if not os.path.isdir(item):
            os.rename(item,"%s/%s" %(test_name,item))

def load_config(config_name):

    with open(config_name, "r") as conf_file:

        conf_file = json.load(conf_file)

    return conf_file

'''
main method
create argument parser to handle command line Arguments
run correct commands for those arguments
'''
def main():

    parser = argparse.ArgumentParser(description="artificial genome reference generator and read simulator",
                                     epilog="the Milli Vanilli of w[rapper] scripts")
    parser.add_argument("config",  metavar="CONFIG.JSON", help="configuration file")
    generator_group = parser.add_mutually_exclusive_group()
    generator_group.add_argument("--gen-reference", dest="GENREFERENCE", action="store_true",help="generate mutant references specified in config file")
    generator_group.add_argument("--gen-reads", dest="GENREADS", action="store_true", help="generate read set from reference specified in config file")
    generator_group.add_argument("--gen-reads-noerr", dest="GENREADSNOERR", action="store_true", help="generate read set from reference specified in config file with NO errors")
    generator_group.add_argument("--gen-config", type=int, dest="N",  help="generate config template with N number of references" )

    args = parser.parse_args()

    if args.N is None:
        with open(args.config, "r") as conf_file:
            conf_file = json.load(conf_file)

        test_vars = conf_file['test variables']
        workind_dir = test_vars['file locations']['working_dir']

        if os.path.isdir(workind_dir):

            os.chdir(workind_dir)

    #build the config template for --gen-config
    if args.N:

        makeconfig = ConfigGen()

        makeconfig.build_config(args.config,args.N)

    #generate reference for --gen-reference
    elif args.GENREFERENCE:

        generate_references(args.config)
        clean_up(args.config)

    #generate reads for --gen-reads
    #assumes on sequence per file
    elif args.GENREADS:

        read_list = generate_reads_from_config(args.config)

        for rset in read_list:

            #for rfile in glob.glob("*"+rset.test_header+"*" ):

            with open(rset.test_header+".fastq", "r") as outFile:

                print(outFile.read())

            os.remove(rset.test_header+".fastq")

    elif args.GENREADSNOERR:

        generate_reads_no_errors(args.config)

    #if none of the above do it all
    #output readsets strings to stdout
    else:

        mutant_list = generate_references(args.config)
        reads_list = generate_reads_from_mutant(args.config,mutant_list)
        count = 0


        summary_json = OrderedDict()
        summary_json["test_name"] = load_config(args.config)["test variables"]["test_name"]
        summary_json["mutant_genome"] = []
        summary_json["mutant_reads"] = []

        #load all the mutants into the summary
        #we don't do this in read building
        #more than one readset can match to same genome
        for mutant in mutant_list:
            tmp_mutant_json = json.loads(mutant.json_summary)
            for rec in tmp_mutant_json["mutant_genome"]:
                summary_json["mutant_genome"].append(rec)

        for rset in reads_list:



            # #for rfile in glob.glob("*"+rset.test_header+"*" ):
            #
            # with open(rset.test_header+".fastq", "r") as outFile:
            #
            #     print(outFile.read())
            tmp_m_json = json.loads(rset.summary_json)

            # append json summary of read to test set summary
            summary_json["mutant_reads"].append(tmp_m_json["mutant_reads"])

            rset_id = tmp_m_json["mutant_reads"]["mutant_read_set"]["id"]
            mutant_origin ="NOTFOUND"
            #print("searching for : {0}".format(rset_id))
            for mutant in mutant_list:
                if mutant.ref_id in rset_id:
                    #print("origin: {0} : mref: {1}".format(mutant.ref_id,rset_id))
                    mutant_origin = mutant.ref_id
                    break

            '''convert fastq files to fasta
                on the fly and output
                they are now in correct format for vedro
            '''
            with open(rset.test_header+".fastq", "r") as outFile:

                for record in SeqIO.parse(outFile, "fastq"):

                    #we can add this more info if we need it
                    #record.id = "{0}:{1}:{2[1]}:{2[2]}:{2[3]}".format(mutant_origin,rset_id,record.id.split("_"))
                    old_header = record.id.split("_")
                    record.id = "{0}:{1}:{2[0]}_{2[1]}:SetPos_{3}".format(mutant_origin,rset_id,old_header,count)
                    record.description = ""
                    print(record.format("fasta").strip("\n"))
                    count += 1
                # for line in outFile.readlines():
                #
                #     if "@Read" in line:
                #         print("@{0}:{1}:{2[1]}:{2[2]}:{2[3]}".format(mutant_origin,rset_id,line.split("_")))
                #     else:
                #         print(line.rstrip("\n"))
                # #print(outFile.read())

        with open(summary_json["test_name"]+".summary.json", "w") as sjson_out:
            json.dump(summary_json,sjson_out)

        clean_up(args.config)

#if argens then argens
if __name__=="__main__":

    main()
