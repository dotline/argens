'''
generate_config.py
ConfigGen class to store template for json formatted config file
class can then generate the config template

Justin Evan Kincaid <jkincaid@nau.edu>
12/2016
'''
import json
from collections import OrderedDict
import sys
from datetime import datetime

'''
class to store template for json formatted config file
class can then generate the config template
'''
class ConfigGen:

    #template for the config file
    #entries can be edited but TAGS should not ever NEVER be changed
    #or argens will break
    #OrderedDict are used to maintain readability and uniformity
    def __init__(self):

        self.__config_root = OrderedDict()

        self.__config_root['test variables'] = OrderedDict()

        self.__config_root['test variables']['test_name'] = "enter name of test"
        self.__config_root['test variables']['file locations'] = OrderedDict()
        self.__config_root['test variables']['file locations']['working_dir'] = "enter working directory"
        self.__config_root['test variables']['file locations']['mutant_reference_dir'] = "directory to store mutants"
        self.__config_root['test variables']['file locations']['tmp_directory'] = "directory for tmp references"
        self.__config_root['test variables']['file locations']['test_set_summary_dir'] = "directory to store test set summaries"
        self.__config_root['test variables']['total_number_of_reads'] = 1000



        self.__config_root['individual params'] =[]

        self.__reference_test = OrderedDict()

        self.__reference_test['reference'] = OrderedDict()

        self.__reference_test['reference']["reference_path"] =   "path/to/reference.{fa,fasta}"
        self.__reference_test['reference']["mutator_params"] =   OrderedDict()
        self.__reference_test['reference']["mutator_params"]["substitution_ratio"] = 0.0133
        self.__reference_test['reference']["mutator_params"]["insertion_ratio"] = 0.0233
        self.__reference_test['reference']["mutator_params"]["deletion_ratio"] = 0.0333
        self.__reference_test['reference']["read_params"] = OrderedDict()
        self.__reference_test['reference']["read_params"]['read_length'] = 50
        self.__reference_test['reference']["read_params"]['total_set_ratio'] = 0.25
        self.__reference_test['reference']["read_params"]['substitution_ratio'] = 0.0144
        self.__reference_test['reference']["read_params"]['insertion_ratio'] = 0.0244
        self.__reference_test['reference']["read_params"]['deletion_ratio'] = 0.0344



    #build config with record for each reference
    def build_config(self, file_name, num_of_references):

        if ".json" in file_name:

            print("found json")
            file_name = file_name.rstrip(".json")

        for i in range(int(num_of_references)):

            self.__config_root['individual params'].append(self.__reference_test)

        if file_name is None:
            file_name = "config_template" + datetime.now().strftime("_%d%m%y%H%M%S")
        else:
            file_name = file_name +"_config_template" + datetime.now().strftime("_%d%m%y%H%M%S")


        with open(file_name+".json" , 'w') as tmp_json:

            json.dump(self.__config_root,tmp_json,indent=4)

        print("%s successfully generated!" %file_name)
